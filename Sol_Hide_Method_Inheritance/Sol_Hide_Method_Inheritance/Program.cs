﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Hide_Method_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Pooja poojaObj = new Pooja();
            poojaObj.TestMethod();

            Mangal mangalObj = new Mangal();
            mangalObj.TestMethod();

            //Rahul rahulObj = new Rahul();  //multi-level inheritance
            //rahulObj.TestMethod();
        }
    }

    public class Pooja
    {
        public virtual void TestMethod()
        {
            Console.WriteLine("pooja method");
        }
    }

    public class Mangal:Pooja
    {
        public new virtual void TestMethod()
        {
            Console.WriteLine("mangal method");
        }
    }

    //public class Rahul : Mangal  //multi-level inheritance
    //{
    //    public new virtual void TestMethod()
    //    {
    //        Console.WriteLine("rahul method");
    //    }
    //}
}
